---
title: "Mystcraft: Ages v0.0.1"
date: 2023-09-30T17:54:08+01:00
draft: false
comments: false
images:
---

# Mystcraft: Ages
A modern reimagining of the original [Mystcraft](https://www.curseforge.com/minecraft/mc-mods/mystcraft) mod!

## Alpha 1
**This is an alpha release, the mod is nowhere near complete and some bugs may exist. If you do find any bugs please report them on [the bug tracker](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/issues)**

### Developer Notes
It's been a while since I said I was gonna start this project and after over 60 hours of work 😅 you now have a semi-functioning proof-of-concept! I'm very efficient I know 😔

Jokes aside, the (albeit limited) content list/changelog is below!

### Changelog
- Linking Books
These are the main attraction of this alpha, and using one can link it so wherever you are standing. There are a few known bugs with these so please check before reporting.
- Book Receptacle
These are small, lectern-like blocks that you can place Linked Books into. (Currently) they allow multiple people to use the same book, but the book will not travel with you.
- Writing Stand
This is where most of the development time has gone and is the place where you can create your Linking Books. It requires you to provide it with an ink supply.
- Ink Vials
A slightly more efficient ink than regular ink sacs.

### Links


[![curseforge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://legacy.curseforge.com/minecraft/mc-mods/mystcraft-ages)
[![modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/datapack/mystcraft)
[![gitlab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages)
[![readthedocs](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/documentation/readthedocs_vector.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages)
[![kofi-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/donate/kofi-singular_vector.svg)](https://ko-fi.com/naomieow)
[![discord-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/social/discord-singular_vector.svg)](http://discord.naomieow.xyz/)

**If the :modrinth: and :curseforge: links are still being approved, you can download the binary from [GitLab](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/jobs/5197870132/artifacts/file/build/libs/mystcraft-ages-0.0.1.jar)**

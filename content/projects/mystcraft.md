---
title: "Mystcraft: Ages"
date: 2023-09-30T17:54:08+01:00
draft: false
comments: false
images:
---

Mystcraft: Ages is a modern rewrite and reimagining of the original [Mystcraft](https://www.curseforge.com/minecraft/mc-mods/mystcraft) mod for Fabric (and eventually Forge).

It is based on the puzzle game franchise "Myst" by Cyan Inc. The mod allows for the creation of a near-infinite amount of dimensions, called Ages, through writing
in Descriptive Books.

---

### 🚧 Currently in development 🚧


[![curseforge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://legacy.curseforge.com/minecraft/mc-mods/mystcraft-ages)
[![modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/datapack/mystcraft)
[![gitlab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages)
[![readthedocs](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/documentation/readthedocs_vector.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages)
[![kofi-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/donate/kofi-singular_vector.svg)](https://ko-fi.com/naomieow)
[![discord-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/social/discord-singular_vector.svg)](http://discord.naomieow.xyz/)

---
title: "RPGTitles"
date: 2022-12-29T21:40:08+01:00
draft: false
comments: false
images:
---

Small, cosmetic datapack that adds titles to small things in the game! See the dimension name when you switch dimensions, see the current biome you are in and maybe get something when you kill a powerful foe..

---

### Configuration Options:
Most aspects of the datapack can be configured using the following commands:   

`/trigger gamerule.repeatBossTitles`  
If enabled, boss titles will be shown every time one is killed, else it is just shown the first time one is killed.

`/trigger gamerule.repeatDimensionTitles`  
If enabled, dimension titles will be shown every time you enter a dimension, else it is just shown the first time.

`/trigger gamerule.showCurrentBiome`  
If enabled, your current biome will be shown in the action bar.

---

### Compatibility:
- [Terralith](https://modrinth.com/mod/terralith): Since v1.1.0
- [Tectonic](https://modrinth.com/datapack/tectonic): Since v1.3.0
- [Incendium](https://modrinth.com/mod/incendium): Planned
- [Nullscape](https://modrinth.com/mod/nullscape): Planned
- [William Wyther's](https://modrinth.com/mod/wwoo): Planned
- [Stellarity](https://modrinth.com/datapack/stellarity): Planned  
And many others are also planned!

---

[![curseforge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://legacy.curseforge.com/minecraft/mc-mods/rpgtitles)
[![modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/datapack/rpgtitles/)
[![gitlab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/naomieow/rpgtitles)
[![kofi-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/donate/kofi-singular_vector.svg)](https://ko-fi.com/naomieow)
[![discord-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/social/discord-singular_vector.svg)](http://discord.naomieow.xyz/)
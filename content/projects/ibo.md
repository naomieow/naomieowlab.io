---
title: "Incendium Biomes Only"
date: 2023-05-06T21:40:08+01:00
draft: false
comments: false
images:
---

This small datapack removes all items, mobs, bosses and structures from the [Incendium](https://modrinth.com/mod/incendium) datapack/mod *without* redistributing any of its code. This is quite often requested in the Stardust Labs Discord server, so I thought I'd finally make this to quiet some people down.

**I am not affiliated with Stardust Labs, and Stardust Labs will offer zero support for this whatsoever. If you need support for this pack, please either use the GitHub issue tracker or join my [Discord](https://discord.gg/KyBzvxwgvw)**

[![modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/datapack/ibo)
[![gitlab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/naomieow/ibo)
[![readthedocs](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/documentation/readthedocs_vector.svg)]()
[![kofi-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/donate/kofi-singular_vector.svg)](https://ko-fi.com/naomieow)
[![discord-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/social/discord-singular_vector.svg)](http://discord.naomieow.xyz/)
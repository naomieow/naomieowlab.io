---
title: "Get Pricked!"
date: 2023-01-16T21:40:08+01:00
draft: false
comments: false
images:
---

Ever been playing the game and thought a block should damage you even though it doesn't? Well, now they do!

### Blocks made prickly:
- Stonecutter
- Rose Bush
- Coral Blocks
- Amethyst Buds and Clusters

[![curseforge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://legacy.curseforge.com/minecraft/mc-mods/get-pricked)
[![modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/datapack/pricked/)
[![gitlab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/naomieow/pricked)
[![readthedocs](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/documentation/readthedocs_vector.svg)]()
[![kofi-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/donate/kofi-singular_vector.svg)](https://ko-fi.com/naomieow)
[![discord-singular](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/social/discord-singular_vector.svg)](http://discord.naomieow.xyz/)